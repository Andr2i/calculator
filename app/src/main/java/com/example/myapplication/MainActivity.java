package com.example.myapplication;


import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.lbl_display)
    TextView display;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_clear)
    public void onClearClick(Button v) {
        String str = display.getText().toString();
        if (str.length() != 0)
            display.setText(str.substring(0, str.length() - 1));

    }

    @OnLongClick(R.id.btn_clear)
    public void onClearLongClick(Button v) {
        display.setText("");

    }

    @OnClick({R.id.btn_number_one, R.id.btn_number_two, R.id.btn_number_three, R.id.btn_number_four,
            R.id.btn_number_five, R.id.btn_number_six, R.id.btn_number_seven, R.id.btn_number_eight,
            R.id.btn_number_nine, R.id.btn_number_zero})
    public void onNumberClick(Button v) {

        display.setText(display.getText() + v.getText().toString());

    }

    @OnClick({R.id.btn_operator_add, R.id.btn_operator_divide, R.id.btn_operator_multiply,
            R.id.btn_operator_subtract})
    public void onOperatorClick(Button v) {
        display.setText(display.getText() + v.getText().toString());
    }

    @OnClick(R.id.btn_decimal)
    public void onDecimalClick(Button v) {
        display.setText(display.getText() + v.getText().toString());
    }

    @OnClick(R.id.btn_evaluate)
    public void onEvaluateClick(Button v) {
        display.setText(v.getText().toString());
    }


}
